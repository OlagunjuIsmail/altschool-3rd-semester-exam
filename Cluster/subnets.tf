resource "aws_subnet" "private-subnet" {
     vpc_id = aws_vpc.ismail-vpc.id
     cidr_block = "10.0.0.0/19"
     availability_zone = "us-east-1a"
     tags = {
        "Name"  = "ismail-private-subnet"
        "kubernetes.io/role/internal-elb" = "1"
        "kubernetes.io/cluster/cluster0" = "owned"

     }
}

resource "aws_subnet" "private-2-subnet" {
     vpc_id = aws_vpc.ismail-vpc.id
     cidr_block = "10.0.32.0/19"
     availability_zone = "us-east-1b"
     tags = {
        "Name" = "ismail-private2-subnet"
        "kubernetes.io/role/internal-elb" = "1"
        "kubernetes.io/cluster/cluster0" = "owned"

     }
}


resource "aws_subnet" "public-subnet" {
     vpc_id = aws_vpc.ismail-vpc.id
     cidr_block = "10.0.64.0/19"
     availability_zone = "us-east-1a"
     map_public_ip_on_launch = true
     tags = {
        "Name" = "ismail-public-subnet"
        "kubernetes.io/role/elb" = "1"
        "kubernetes.io/cluster/cluster0" = "owned"

     }
}

resource "aws_subnet" "public-2-subnet" {
     vpc_id = aws_vpc.ismail-vpc.id
     cidr_block = "10.0.96.0/19"
     availability_zone = "us-east-1b"
     map_public_ip_on_launch = true
     tags = {
        "Name" = "ismail-public-2-subnet"
        "kubernetes.io/role/elb" = "1"
        "kubernetes.io/cluster/cluster0" = "owned"

     }
}

