
resource "aws_vpc" "ismail-vpc" {
    cidr_block = "10.0.0.0/16"
    tags = {
        Name : "ismail-vpc"

    }
    
}


resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.ismail-vpc.id
    tags = {
    Name: "ismail-igw"
  }
}


resource "aws_eip" "nat" {
    vpc = true
    
    tags = {
        Name: "nat"
    }
}

resource "aws_nat_gateway" "nat-gw"{
    allocation_id = aws_eip.nat.id
    subnet_id = aws_subnet.public-subnet.id

    tags = {
        Name: "ismail-nat-gw"
    }

    depends_on = [aws_internet_gateway.igw]
}