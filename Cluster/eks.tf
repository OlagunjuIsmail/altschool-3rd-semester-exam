resource "aws_iam_role" "role" {
  name = "cluster0"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
    policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
    role = aws_iam_role.role.name

}

resource "aws_eks_cluster" "cluster0" {
    role_arn = aws_iam_role.role.arn
    name = "cluster0"

    vpc_config {
        subnet_ids = [
            aws_subnet.private-subnet.id,
            aws_subnet.private-2-subnet.id,
            aws_subnet.public-subnet.id,
            aws_subnet.public-2-subnet.id,

        ]
    }

    depends_on = [aws_iam_role_policy_attachment.AmazonEKSClusterPolicy]

}