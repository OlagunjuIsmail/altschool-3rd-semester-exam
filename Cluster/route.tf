
resource "aws_route_table" "private-rtb" {
  vpc_id = aws_vpc.ismail-vpc.id

  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      nat_gateway_id             = aws_nat_gateway.nat-gw.id
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      gateway_id                 = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    },
  ]

  tags = {
    Name = "private-rtb"
  }
}

resource "aws_route_table" "public-rtb" {
  vpc_id = aws_vpc.ismail-vpc.id

  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      gateway_id                 = aws_internet_gateway.igw.id
      nat_gateway_id             = ""
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    },
  ]

  tags = {
    Name = "public-rtb"
  }
}

resource "aws_route_table_association" "private-1-ass" {

    subnet_id = aws_subnet.private-subnet.id
    route_table_id = aws_route_table.private-rtb.id
   
}

resource "aws_route_table_association" "private-2-ass" {
    
    subnet_id = aws_subnet.private-2-subnet.id
    route_table_id = aws_route_table.private-rtb.id
   
}


resource "aws_route_table_association" "public-1-ass" {
    
    subnet_id = aws_subnet.public-subnet.id
    route_table_id = aws_route_table.public-rtb.id
   
}


resource "aws_route_table_association" "public-2-ass" {
    
    subnet_id = aws_subnet.public-2-subnet.id
    route_table_id = aws_route_table.public-rtb.id
   
}