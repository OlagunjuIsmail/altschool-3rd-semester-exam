#!/bin/bash

kubectl apply -f cart.yaml
kubectl apply -f catalogue.yaml
kubectl apply -f front-end.yaml
kubectl apply -f orders.yaml
kubectl apply -f payment.yaml
kubectl apply -f queue-master.yaml
kubectl apply -f rabbitmq.yaml
kubectl apply -f session-db.yaml
kubectl apply -f shipping.yaml

kubectl apply -f user.yaml
