#!/bin/bash

# Apply the backend deployment and service YAML files
kubectl apply -f voting-app-backend.yaml
kubectl apply -f backend-service.yaml

# Apply the frontend deployment and service YAML files
kubectl apply -f voting-app-frontend.yaml
kubectl apply -f frontend-service.yaml


